DXCloud PHP SDK
============================

The DXCloud SDK is designed to greatly simplify the generation of documents via
the DXcloud FREE REST API Service.

This SDK contains a series of convenience methods to:

*  Merge text/HTML content into a document: plaint text, tables, lists, etc
*  Remove blocks of content
*  Clone blocks of content
*  Manage chart data

You may find a detailed documentation about the use of the REST API and the 
SDK in:

*  [REST API Documentation](https.//docxpresso.cloud/docs/developers)
*  [PHP SDK](https.//docxpresso.cloud/docs/developers)
 
## Installation

The recommended way to install the SDK is through
[Composer](http://getcomposer.org).

```bash
# Install Composer
curl -sS https://getcomposer.org/installer | php
```

Next, run the Composer command to install the latest stable version of the SDK:

```bash
php composer.phar require docxpresso/dxcloud
```

Or just download this package, uncompress it in the folder of your choice and
run:

```bash
php composer.phar install
```

## Using the SDK

The use of the SDK is extremely simple:

```php
<?php
//path_to_vendor is either the full or relative path to the vendor folder
require_once('path_to_vendor/vendor/autoload.php');
use DXCloud\SDK as SDK;
//call the constructor including your API key
$DX = new SDK\DXClient('apikey');
//get the required access token. Beware that the token expires after an hour
$DX->generateAccessToken();
//create the document giving the path of the template or a base64 encoded string
$document = $DX->createDocument('path_to_template.odt');
//replace a variable value (methods can be chained)
$document->replaceText('company', ['Docxpresso'])
         ->replaceText('address', ['Arturo Baldasano 16, <strong>SPAIN</strong>']);
//render the rdocument by making a request to the REST API
$document->render('saveAndDownload', 'pdf', 'folder/filename.pdf');  
```

Please visit our [DXCloud website](https://docxpresso.cloud) to register, start
using our FREE REST API and get acquainted with all the required details.
