<?php

namespace DXCloud\SDK;

use Monolog\Logger;

class Log
{

    /**
     * log
     * @access private
     * @static
     * @var string
     */
    private static $_log = NULL;
    private static $_path;

    private static $levels = array(
        'debug',
        'info',
        'notice',
        'warning',
        'error',
        'critical',
    );

    private function __construct()
    {
        self::$_log = new Logger('dxcloud');
        if (self::$_path == "") {
            self::$_path = dirname(__FILE__) . '/../log/dxcloud.log';
        }

        self::$_log->pushHandler(new \Monolog\Handler\StreamHandler(
            self::$_path,
            \Monolog\Logger::DEBUG
        ));

        return self::$_log;
    }

    public static function logger($message, $level)
    {
        if (!self::$_log) {
            new Log();
        }

        // only some leves are valids
        if (in_array($level, self::$levels)) {
            $stringLevel = 'add' . ucfirst($level);
            self::$_log->$stringLevel($message);
        }
    }

    public static function setPath($path)
    {
        if (!self::$_log) {
            new Log();
        }
        self::$_path = $path;

        self::$_log->popHandler();
        self::$_log->pushHandler(new \Monolog\Handler\StreamHandler(
            $path,
            \Monolog\Logger::DEBUG
        ));
    }

    public static function getPath()
    {
        if (self::$_path == "") {
            self::$_path = dirname(__FILE__) . '/../log/dxcloud.log';
        }
        return self::$_path;
    }

}