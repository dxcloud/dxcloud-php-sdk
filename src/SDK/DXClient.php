<?php

namespace DXCloud\SDK;

use DXCloud\SDK\Log;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Class DXClient
 * A collection of methods that simplify the generation of documents via the 
 * DXCloud API REST
 */
class DXClient
{
    private $data;
    private $apikey;
    private $apiendpoint = 'https://api.docxpresso.cloud/';
    private $response;
    private $responseTries;
    private $errorFlag;
    private $mimeTypeArray = array(
        "pdf" => "application/pdf",
        "doc" => "application/msword",
        "odt" => "application/vnd.oasis.opendocument.text",
        "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "rtf" => "application/rtf"
    );

    private $timeout = 200.0;
    private $token;
    private $logLevel = 'info'; // info|error|none

    /**
     * Constructor
     *
     * @param string $apikey
     * @param string $endpoint if null will use the default endpoint
     *
     * @access public
     */
    public function __construct($apikey, $endpoint = null)
    {
        $this->responseTries = 0;

        $this->apikey = $apikey;
        if (empty($endpoint)) {
            $endpoint = $this->apiendpoint;
        } else {
            $this->apiendpoint = $endpoint;
        }
        if ($this->logLevel == "info") {
            Log::logger("Object created", "info");
        }
    }

    /**
     * Takes a file and returns it as a base64 encoded string
     *
     * @param string $filename
     * @return string
     *
     * @access public
     */

    public static function getBase64File($filename)
    {
        //
        $data = base64_encode(file_get_contents($filename));
        return $data;
    }

    /**
     * Creates a file from a base64 encoded string
     *
     * @param string $content
     * @param string $filename
     * @return boolean
     *
     * @access public
     */

    public static function createBase64File($content, $filename)
    {

        if (file_exists($filename)) {
            unlink($filename);
        }

        file_put_contents($filename, base64_decode($content));

        return file_exists($filename);
    }

    /**
     * Fetches an Access Token from the REST API and stores it so it can be
     * reused in multiple requests within a single script.
     * IMPORTANT: access tokens expire after 3600 seconds
     *
     * @return string
     * @access public
     */
    public function generateAccessToken()
    {
        if ($this->logLevel == 'info') {
            Log::logger("Fetching access token", "info");
        }
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->apiendpoint,
            // You can set any number of default request options.
            'timeout' => $this->timeout,
        ]);
        try {
            $response = $client->request('GET', '/generateAccessToken',
                [
                    'headers' => [
                        'X-Api-Key' => $this->apikey,
                        'Accept' => 'application/json',
                    ]
                ]);

            if ($response->getStatusCode() != 200) {
                // someting bad happend
                if ($this->logLevel != 'none') {
                    Log::logger($response->getReasonPhrase(), "error");
                }

            }

            $stream = $response->getBody();
            $stream->rewind(); // Seek to the beginning
            $contents = $stream->getContents(); // returns all the contents

            $bodyJson = json_decode($contents);

            $token = $bodyJson->access_token;
            if ($this->logLevel == 'info') {
                Log::logger("Token: " . $bodyJson->access_token, "info");
            }

            $this->token = $token;
            return $token;

        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                if ($this->logLevel != 'none') {
                    Log::logger(Psr7\str($e->getResponse()), "error");
                }

                echo Psr7\str($e->getResponse());
            }

            return "";
        }

    }

    /**
     * Returns the json encoded object that represents template data from a file.
     * This object is used to send to the API.
     * You just need to call to this method if you need to check what is sent to
     * the API endpoint
     *
     * @return string
     */
    public function getData()
    {
        return json_encode($this->data);
    }

    /**
     * Replace all variable appearances of a field with the values included in
     * the varValues array.
     * The replacement algorithm follows a cyclic rule.
     *
     * @param string $varName the name of the variable to be replaced
     * @param array $varValues an array with the replacement values
     * @param array $options with the following key => value pairs:
     *  'html' => (boolean) if true the string will be parsed as HTML
     *  (default is true).
     *  'parse-line-breaks' => (boolean)if true parses line breaks
     *   (default is false).
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'block-type' => (boolean) if true the containing paragraph will be
     *   completely replaced by the provided value (default is false).
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     *
     * @return object DXClient
     */
    public function replaceText($varName, $varValues, $options = array())
    {
        $this->_replaceBlock($varName, $varValues, "text", $options);
        return $this;
    }

    /**
     * This method combines the clone method for paragraphs with the plain text
     * merging to create as many paragraphs as needed with the required content
     *
     * @param array $data an array of arrays where the key must correspond to
     * the variable name and the value should be itself an array with the values
     * that should be fed, i.e.
     * array('var_1' => array('val1', 'val2')), 'var2' => array('val3', 'val4'))
     * @param array $options with the following key => value pairs:
     *  'html' => (boolean) if true the string will be parsed as HTML
     *  (default is true).
     *  'parse-line-breaks' => (boolean)if true parses line breaks
     *   (default is false).
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function replaceParagraph($data, $options = array())
    {
        $varsObject = new \stdClass();
        $varsObject->vars = array();
        $varsObject->options = new \stdClass();

        foreach ($data as $key => $values) {

            $varObject = new \stdClass();

            $varObject->var = $key;
            $varObject->value = $values;

            $varsObject->vars[] = $varObject;
        }

        foreach ($options as $key => $value) {
            $varsObject->options->{$key} = $value;
        }

        $varsObject->options->element = "paragraph";

        $this->data->replace[] = $varsObject;

        return $this;
    }

    /**
     * This method clones a list item as many times as needed in order
     * to print all values included in the varValues array.
     *
     * @param string $varName the name of the variable to be replaced
     * @param array $varValues an array with the replacement values
     * @param array $options with the following key => value pairs:
     *  'html' => (boolean) if true the string will be parsed as HTML
     *  (default is true).
     *  'parse-line-breaks' => (boolean)if true parses line breaks
     *   (default is false).
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     *
     * @return object DXClient
     */
    public function replaceListItem($varName, $varValues, $options = array())
    {
        $this->_replaceBlock($varName, $varValues, "list", $options);
        return $this;
    }


    /**
     * This method clones as many times as needed the block of rows including
     * the provided variable values
     * @param array $data an array of arrays where the key must correspond to
     * the variable name and the value should be itself an array with the values
     * that should be fed, i.e.
     * array('var_1' => array('val1', 'val2')), 'var2' => array('val3', 'val4'))
     *
     * @param array $options with the following key => value pairs:
     *  'html' => (boolean) if true the string will be parsed as HTML
     *  (default is true).
     *  'parse-line-breaks' => (boolean)if true parses line breaks
     *   (default is false).
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'block-type' => (boolean) if true the containing paragraph will be
     *   completely replaced by the provided value (default is false).
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function replaceTable($data, $options = array())
    {
        $varsObject = new \stdClass();
        $varsObject->vars = array();
        $varsObject->options = new \stdClass();

        foreach ($data as $key => $values) {

            $varObject = new \stdClass();

            $varObject->var = $key;
            $varObject->value = $values;

            $varsObject->vars[] = $varObject;
        }

        foreach ($options as $key => $value) {
            $varsObject->options->{$key} = $value;
        }

        $varsObject->options->element = "table";

        $this->data->replace[] = $varsObject;

        return $this;
    }


    /**
     * This method replaces an image that has been tagged with an alt text.
     * You may use a path for the new image or directly provide a base64 encoded
     * image, i.e. data:image/png;base64,iVBORw0K… (https://en.wikipedia.org/wiki/Data_URI_scheme)
     * If we leave empty the width and height of the image, the system will use that of the provided image.
     *
     * @param string $varName the name of the variable to be replaced
     * @param array $varValues an array with the replacement values/paths
     * @param string $width the width of the image in px, pt, cm or in. If not
     * provided the width of the provided image will be used.
     * @param string $height the width of the image in px, pt, cm or in. If not
     * provided the height of the prvided image will be used.
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     *
     * @return bool|object DXClient
     */
    public function replaceImage($varName, $varValues, $options = array(), $height = "", $width = "")
    {
        /*
         * data:image/png;base64,
         */
        $auxVarValues = array();
        foreach ($varValues as $value) {
            if (strpos($value, 'data:image/png;base64,') === false) {
                /*
                 * Assume path
                 * check extension jpg, jpeg, png o gif
                 */
                if (file_exists($value)) {
                    $path_parts = pathinfo($value);

                    if (in_array($path_parts['extension'], array("jpg", "jpeg", "png", "gif"))) {
                        $imageBase64 = $this->getBase64File($value);
                        $newVal = 'data:image/png;base64,' . $imageBase64;
                        $auxVarValues[] = $newVal;
                    } else {
                        if ($this->logLevel != 'none') {
                            Log::logger("File is not image", "error");
                        }

                        return false;
                    }
                } else {
                    if ($this->logLevel != 'none') {
                        Log::logger("File not exists", "error");
                    }

                    return false;
                }


            } else {
                $auxVarValues[] = $value;
            }
        }
        $varValues = $auxVarValues;

        $varsObject = new \stdClass();
        $varsObject->vars = array();
        $varsObject->options = new \stdClass();

        $varObject = new \stdClass();

        $varObject->var = $varName;
        $varObject->value = $varValues;

        if ($height != "") {
            $varObject->height = $height;
        }
        if ($width != "") {
            $varObject->width = $width;
        }


        $varsObject->vars[] = $varObject;

        foreach ($options as $key => $value) {
            $varsObject->options->{$key} = $value;
        }

        $varsObject->options->element = 'image';

        $this->data->replace[] = $varsObject;
        return $this;
    }

    /**
     *
     * @param string $template this can be a path or a base64 encoded odt document
     * @return object DXClient
     */
    public function createDocument($template)
    {
        $data = new \stdClass();
        $data->template = $this->_checkTemplate($template);
        $data->output = ""; // default odt
        $data->replace = array();
        $data->remove = array();
        $data->clone = array();
        $data->charts = array();

        $this->data = $data;
        return $this;
    }

    /**
     * Clones the bookmark block containing the provided needle or with
     * the corresponding match value
     *
     * @param string $needle the text to be searched.
     * @param integer $repeat the number of times this block should be cloned.
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     *
     * @return object DXClient
     */
    public function cloneBookmark($needle = "", $repeat, $options = array())
    {
        $this->_cloneContent($needle, "bookmark", $repeat, $options);
        return $this;
    }

    /**
     * Clones the paragraph block containing the provided needle or with
     * the corresponding match value
     *
     * @param string $needle the text to be searched.
     * @param integer $repeat the number of times this block should be cloned.
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function cloneParagraph($needle = "", $repeat, $options = array())
    {
        $this->_cloneContent($needle, "paragraph", $repeat, $options);
        return $this;
    }

    /**
     * Clones the list containing the provided needle or with
     * the corresponding match value
     *
     * @param string $needle the text to be searched.
     * @param integer $repeat the number of times this block should be cloned.
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function cloneList($needle = "", $repeat, $options = array())
    {
        $this->_cloneContent($needle, "list", $repeat, $options);
        return $this;

    }

    /**
     * Clones the list item containing the provided needle or with
     * the corresponding match value
     *
     * @param string $needle the text to be searched.
     * @param integer $repeat the number of times this block should be cloned.
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function cloneListItem($needle = "", $repeat, $options = array())
    {
        $this->_cloneContent($needle, "list-item", $repeat, $options);
        return $this;
    }

    /**
     * Clones the table group containing the provided needle or with
     * the corresponding match value
     *
     * @param string $needle the text to be searched.
     * @param integer $repeat the number of times this block should be cloned.
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function cloneTable($needle = "", $repeat, $options = array())
    {
        $this->_cloneContent($needle, "table", $repeat, $options);
        return $this;
    }

    /**
     * Clones the table row containing the provided needle or with
     * the corresponding match value
     *
     * @param string $needle the text to be searched.
     * @param integer $repeat the number of times this block should be cloned.
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function cloneTableRow($needle = "", $repeat, $options = array())
    {
        $this->_cloneContent($needle, "table-row", $repeat, $options);
        return $this;

    }

    /**
     * Clones the image containing the provided needle (as an alt text) or with
     * the corresponding match value
     *
     * @param string $needle the text to be searched in the image alt text
     * @param integer $repeat the number of times this block should be cloned.
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function cloneImage($needle = "", $repeat, $options = array())
    {
        $this->_cloneContent($needle, "image", $repeat, $options);
        return $this;

    }

    /**
     * Clones the chart containing the provided needle (provide as alt text) or
     * with the corresponding match value
     *
     * @param string $needle the text to be searched as an alt text.
     * @param integer $repeat the number of times this block should be cloned.
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function cloneChart($needle = "", $repeat, $options = array())
    {
        $this->_cloneContent($needle, "chart", $repeat, $options);
        return $this;
    }

    /**
     * Clones the textbox containing the provided needle or with
     * the corresponding match value
     *
     * @param string $needle the text to be searched.
     * @param integer $repeat the number of times this block should be cloned.
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function cloneTextBox($needle = "", $repeat, $options = array())
    {
        $this->_cloneContent($needle, "textbox", $repeat, $options);
        return $this;

    }

    /**
     * Clones the full set of contents that go under a given heading
     *
     * @param string $needle the text to be searched in the heading.
     * @param integer $repeat the number of times this block should be cloned.
     * @param array $options with the following key => value pairs:
     *  'heading-level' => (integer) the heading level to be searched. The
     *   default value is 1.
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function cloneHeading($needle = "", $repeat, $options = array())
    {
        $this->_cloneContent($needle, "heading", $repeat, $options);
        return $this;
    }

    /**
     * Removes the plain text provided as needle
     *
     * @param string $needle text to be searched
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the text should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function removeText($needle, $options = array())
    {
        $this->_removeContent($needle, "text", $options);
        return $this;
    }

    /**
     * Removes the bookmark containing the provided needle or if empty the
     * provided match
     *
     * @param string $needle text to be searched
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the bookmark should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function removeBookmark($needle = "", $options = array())
    {
        $this->_removeContent($needle, "bookmark", $options);
        return $this;
    }

    /**
     * Removes the paragraph containing the provided needle or if empty the
     * provided match
     *
     * @param string $needle text to be searched
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the paragraph should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function removeParagraph($needle = "", $options = array())
    {
        $this->_removeContent($needle, "paragraph", $options);
        return $this;
    }

    /**
     * Removes the list containing the provided needle or if empty the
     * provided match
     *
     * @param string $needle text to be searched
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the list should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function removeList($needle = "", $options = array())
    {
        $this->_removeContent($needle, "list", $options);
        return $this;
    }

    /**
     * Removes the list item containing the provided needle or if empty the
     * provided match
     *
     * @param string $needle text to be searched
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the list item should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function removeListItem($needle = "", $options = array())
    {
        $this->_removeContent($needle, "list-item", $options);
        return $this;
    }

    /**
     * Removes the table containing the provided needle or if empty the
     * provided match
     *
     * @param string $needle text to be searched
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the table should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function removeTable($needle = "", $options = array())
    {
        $this->_removeContent($needle, "table", $options);
        return $this;
    }

    /**
     * Removes the table row containing the provided needle or if empty the
     * provided match
     *
     * @param string $needle text to be searched
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the table row should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function removeTableRow($needle = "", $options = array())
    {
        $this->_removeContent($needle, "table-row", $options);
        return $this;
    }

    /**
     * Removes the image containing the provided needle as an alt text or if
     * empty the provided match
     *
     * @param string $needle alt text to be searched
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the image should
     *   be searched for (default is document).
     *  'container' => (boolean) if true it also removes the parent container
     *   (default value is false)
     * @return object DXClient
     */
    public function removeImage($needle = "", $options = array())
    {
        $this->_removeContent($needle, "image", $options);
        return $this;
    }

    /**
     * Removes the chart containing the provided needle as an alt text or if
     * empty the provided match
     *
     * @param string $needle alt text to be searched
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the chart should
     *   be searched for (default is document).
     *  'container' => (boolean) if true it also removes the parent container
     *   (default value is false)
     * @return object DXClient
     */
    public function removeChart($needle = "", $options = array())
    {
        $this->_removeContent($needle, "chart", $options);
        return $this;
    }

    /**
     * Removes the textbox containing the provided needle or if empty the
     * provided match
     *
     * @param string $needle text to be searched
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the textbox should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function removeTextBox($needle = "", $options = array())
    {
        $this->_removeContent($needle, "textbox", $options);
        return $this;
    }

    /**
     * Searches for a heading cotaining the provided needle, or if empty the
     * provided match, and removes all content falling under it
     *
     * @param string $needle text to be searched
     * @param array $options with the following key => value pairs:
     *  'heading-level' => (integer) the heading level to be searched. The
     *   default value is 1.
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     *  'target' => (document|header|footer)  defines where the variable should
     *   be searched for (default is document).
     * @return object DXClient
     */
    public function removeHeading($needle = "", $options = array())
    {
        $this->_removeContent($needle, "heading", $options);
        return $this;
    }

    /**
     * Replaces Pie (or Doughnut) chart with varName alt text
     *
     * @param string $varName alt text to be searched for
     * @param array $varValues an associative array containing the category
     * names as keys, i.e. ['first' => 20, 'second' => 30]
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     * @return object DXClient
     *
     */
    public function replacePieChart($varName, $varValues, $options = array())
    {
        $dataObject = new \stdClass();
        foreach ($varValues as $key => $value) {
            $dataObject->{$key} = $value;
        }
        $this->_replaceChart($varName, $varValues, $options);
        return $this;
    }

    /**
     * Replaces a generic chart (bar, columns, ..) with varName alt text
     *
     * @param string $varName alt text to be searched for
     * @param array $varValues an associative array containing the series names
     * as first entry followed by the corersponding categories values
     * ["series"=>["ser 1","ser 2","ser 3"], "first"=>[1,3,58],"second"=>[18,5,8]]
     * @param array $options with the following key => value pairs:
     *  'match' => (integer) optional parameter. If given only that match will
     *   be replaced.
     * @return object DXClient
     */
    public function replaceGenericChart($varName, $varValues, $options = array())
    {
        $dataObject = new \stdClass();
        foreach ($varValues as $key => $value) {
            $dataObject->{$key} = $value;
        }
        $this->_replaceChart($varName, $varValues, $options);
        return $this;
    }

    /**
     * Sends a request to Docxpresso Cloud API to generate a new document
     * with all provided data and processes the response
     *
     * @param string $output this may take the following values:
     *  'file': saves the response as a file with the provided
     *   output name.
     *  'string': returns the document as a base64 encoded string.
     *  'browser': sends directly the document to the browser. If the browser
     *   does not recognized the mime type it will (normally) download it.
     *  'download': downloads it directly with the provided name.
     *  'saveAndDonwload': saves it to the file system and sends it to the
     *   browser to be downloaded.
     * @param string $format the required format
     * @param string $name the generated file's name
     *
     * @return bool|object DXClient|string
     */
    public function render($output, $format = 'odt', $name = "")
    {
        $this->errorFlag = false;
        if ($this->logLevel == 'info') {
            Log::logger("generateDocument!", "info");
        }


        if (empty($this->data->template)) {
            if ($this->logLevel != 'none') {
                Log::logger("Template not found ", "error");
            }

            return $this;
        }

        $this->data->output = $format; // default odt

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->apiendpoint,
            // You can set any number of default request options.
            'timeout' => $this->timeout,
        ]);

        $dataPost = $this->getData();

        try {
            $response = $client->request('POST', '/generateDocument',
                [
                    'headers' => [
                        'Authorization' => "Bearer " . $this->token,
                        'Accept' => 'application/json',
                    ],
                    'body' => $dataPost
                ]);

            $this->responseTries += 1;
            $this->response = $response;
            /*
             * Process response
             */
            $stream = $this->response->getBody();
            $stream->rewind(); // Seek to the beginning
            $contents = $stream->getContents(); // returns all the contents

            $bodyJson = json_decode($contents);

            $document = $bodyJson->document;
            if (!isset($document)) {
                // Error
                $this->_processErrorCode($bodyJson);
                if ($this->errorFlag) {
                    if ($this->logLevel != 'none') {
                        Log::logger("Error generating document", "error");
                    }

                } else {
                    if ($this->logLevel == 'info') {
                        Log::logger("Resend generating document", "warning");
                    }

                    $this->errorFlag = false;

                    $response = $client->request('POST', '/generateDocument',
                        [
                            'headers' => [
                                'Authorization' => "Bearer " . $this->token,
                                'Accept' => 'application/json',
                            ],
                            'body' => $dataPost
                        ]);

                    $this->responseTries += 1;
                    $this->response = $response;
                    /*
                     * Process response
                     */
                    $stream = $this->response->getBody();
                    $stream->rewind(); // Seek to the beginning
                    $contents = $stream->getContents(); // returns all the contents

                    $bodyJson = json_decode($contents);

                    $document = $bodyJson->document;
                    if (!isset($document)) {
                        $this->_processErrorCode($bodyJson);
                        $this->errorFlag = true;
                    }
                }
            }

            return $this->_saveDocument($output, $name);

        } catch (RequestException $e) {
            echo Psr7\str($e->getRequest());
            if ($this->logLevel != 'none') {
                Log::logger(Psr7\str($e->getRequest()), "error");
            }


            if ($e->hasResponse()) {
                if ($this->logLevel != 'none') {
                    Log::logger(Psr7\str($e->getResponse()), "error");
                }

                echo Psr7\str($e->getResponse());
            } else {
                if ($this->logLevel != 'none') {
                    Log::logger("Unknown error", "error");
                }

            }

            return $this;
        }
    }

    /**
     * Manages the output of the REST API if succesful otherwise returns false.
     *
     * @param string $output this string may take the following values:
     *  'file': saves the respone as a file with the provided
     *   output name.
     *  'string': returns the document as a base64 encoded string.
     *  'browser': sends directly the document to the browser. If the browser
     *   does not recognized the mime type it will (normally) download it.
     *  'download': downloads it directly with the provided name.
     *  'saveAndDownload': saves it to the file system and sends it to the
     *   browser to be downloaded.
     * @param string $outputName the name or full path where we want the
     *  resulting document to be stored
     *
     * @return bool|string
     */
    private function _saveDocument($output, $outputName = "")
    {
        if (!in_array($output, array("file", "string", "browser", "download", "saveAndDownload"))) {
            if ($this->logLevel != 'none') {
                Log::logger("Output type not available, check its name", "error");
            }
            return false;
        }
        if (in_array($output, array("file", "browser", "download", "saveAndDownload")) && $outputName == "") {
            if ($this->logLevel != 'none') {
                Log::logger("Empty file name", "error");
            }

            return false;
        }

        $return = true;
        if (isset($this->response) && $this->errorFlag == false) {

            $stream = $this->response->getBody();
            $stream->rewind(); // Seek to the beginning
            $contents = $stream->getContents(); // returns all the contents

            $bodyJson = json_decode($contents);

            if (isset($bodyJson->document)) {
                $document = $bodyJson->document;

                switch ($output) {
                    case "file":
                        return $this->createBase64File($document, $this->_correctFormat($outputName));
                        break;
                    case "string":
                        return $document;
                        break;
                    case "browser":
                        $extension = $this->data->output;
                        header("Content-type: " . $this->mimeTypeArray[$extension]);
                        header("Content-disposition: inline; filename=\"" . basename($outputName) . "\"");
                        echo base64_decode($document);
                        break;
                    case "download":
                        $basename = basename($this->_correctFormat($outputName));
                        header('Content-Disposition: attachment; filename=' . $basename);
                        echo base64_decode($document);
                        exit;
                        break;
                    case "saveAndDownload":
                        $this->createBase64File($document, $this->_correctFormat($outputName));
                        $basename = basename($this->_correctFormat($outputName));
                        header('Content-Disposition: attachment; filename=' . $basename);
                        echo base64_decode($document);
                        exit;
                        break;
                    default:
                        $return = false;
                        return false;
                        break;
                }

            } else {
                $return = false;
                return false;
            }
        } else {
            $return = false;
            return false;
        }

        return $return;
    }

    /**
     * Returns the error message from request.
     *
     * @return bool|object
     */
    public function getError()
    {
        if ($this->errorFlag) {
            if (isset($this->response)) {
                $stream = $this->response->getBody();
                $stream->rewind(); // Seek to the beginning
                $contents = $stream->getContents(); // returns all the contents

                $bodyJson = json_decode($contents);

                return $bodyJson;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Sets the JSON data to be sent. This method would be most usually used
     * internally by the SDK so must probably you will have no need to use it.
     *
     * @param \stdClass $jsonObject
     * @return object DXClient
     */
    public function setData($jsonObject)
    {
        $this->data = $jsonObject;
        return $this;
    }

    /**
     * Sets the Log config
     * 
     * @param string $level this may take following values:
     * 'none': No message to be logged
     * 'error': Only log error messages
     * 'info': All messages are logged: info, warning, error
     * @param string $path (optional) absolute path for the log file
     * i.e. something like /var/logs/dxclog.log . If not given the default
     * relative path lod/dxcloud.log will be used
     * 
     */
    public function setLogConfig($level = 'info', $path='')
    {
        $this->logLevel = $level;
        Log::setPath($path);
    }

    /**
     * This method returns a json encoded object with two props: level and path.
     * 
     * @return string
     */
    public function getLogConfig(){
        $result = new \stdClass();

        $result->level = $this->logLevel;
        $result->path = Log::getPath();

        return json_decode($result);
    }

    /**
     * Sends a request to Docxpresso Cloud API to convert a document
     *
     * @param string $source the path of the document to be converted (odt, doc, docx or rtf)
     * @param string $target the name of the converted document (pdf, odt, doc, docx or rft)
     * @param string $output this  may take the following values:
     *  'file': saves the response as a file with the provided
     *   output name.
     *  'string': returns the document as a base64 encoded string.
     *  'browser': sends directly the document to the browser. If the browser
     *   does not recognized the mime type it will (normally) download it.
     *  'download': downloads it directly with the provided name.
     *  'saveAndDonwload': saves it to the file system and sends it to the
     *   browser to be downloaded.
     * @return bool|object DXClient
     */
    public function convert($source, $target, $output)
    {
        $this->errorFlag = false;

        $file_parts = pathinfo($source);
        $inputExtension = $file_parts['extension'];

        $source = $this->_checkTemplate($source);

        if ($this->logLevel == 'info') {
            Log::logger("convertDocument!", "info");
        }

        $data = new \stdClass();

        $file_parts = pathinfo($target);
        $outputExtension = $file_parts['extension'];

        $data->input = $inputExtension; //??
        $data->output = $outputExtension;
        $data->source = $source;

        $this->data = $data;

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->apiendpoint,
            // You can set any number of default request options.
            'timeout' => $this->timeout,
        ]);

        $dataPost = json_encode($data);

        try {
            $response = $client->request('POST', '/convertDocument',
                [
                    'headers' => [
                        'Authorization' => "Bearer " . $this->token,
                        'Accept' => 'application/json',
                    ],
                    'body' => $dataPost
                ]);


            $this->response = $response;
            /*
             * Process response
             */
            $stream = $this->response->getBody();
            $stream->rewind(); // Seek to the beginning
            $contents = $stream->getContents(); // returns all the contents

            $bodyJson = json_decode($contents);

            $document = $bodyJson->document;
            if (!isset($document)) {
                // Error
                $this->_processErrorCode($bodyJson);
                if ($this->errorFlag) {
                    if ($this->logLevel != 'none') {
                        Log::logger("Error generating document", "error");
                    }

                } else {
                    if ($this->logLevel == 'info') {
                        Log::logger("Resend generating document", "warning");
                    }

                    $this->errorFlag = false;

                    $response = $client->request('POST', '/generateDocument',
                        [
                            'headers' => [
                                'Authorization' => "Bearer " . $this->token,
                                'Accept' => 'application/json',
                            ],
                            'body' => $dataPost
                        ]);

                    $this->responseTries += 1;
                    $this->response = $response;
                    /*
                     * Process response
                     */
                    $stream = $this->response->getBody();
                    $stream->rewind(); // Seek to the beginning
                    $contents = $stream->getContents(); // returns all the contents

                    $bodyJson = json_decode($contents);

                    $document = $bodyJson->document;
                    if (!isset($document)) {
                        $this->_processErrorCode($bodyJson);
                        $this->errorFlag = true;
                    }
                }
            }

            return $this->_saveDocument($output, $target);

        } catch (RequestException $e) {

            echo Psr7\str($e->getRequest());
            if ($this->logLevel != 'none') {
                Log::logger(Psr7\str($e->getRequest()), "error");
            }
            if ($e->hasResponse()) {
                if ($this->logLevel != 'none') {
                    Log::logger(Psr7\str($e->getResponse()), "error");
                }
                echo Psr7\str($e->getResponse());
            } else {
                if ($this->logLevel != 'none') {
                    Log::logger("Unknown error", "error");
                }

            }

            return $this;
        }
    }

    /**
     *
     * Returns the json encoded object that represents template data from a file.
     * This object is used to send to the API.
     * You just need to call to this method if you need to check what is sent to
     * the API endpoint
     *
     * If jsonData is set, sets the JSON data to be sent. This method would be most usually used
     * internally by the SDK so must probably you will have no need to use it.
     *
     * @param null $jsonData
     * @return string
     */
    public function data($jsonData = null)
    {
        if (!empty($jsonData)) {
            $this->setData($jsonData);
        }
        return $this->getData();
    }


    /**
     * Replace generic chart
     *
     * @param string $varName
     * @param $dataJson
     * @param array $options
     */
    private function _replaceChart($varName, $dataJson, $options)
    {
        $chartObject = new \stdClass();
        $chartObject->var = $varName;
        $chartObject->data = $dataJson;

        foreach ($options as $key => $value) {
            $chartObject->options->{$key} = $value;
        }

        $this->data->charts[] = $chartObject;
    }

    /**
     * Replace generic block
     *
     * @param string $varName
     * @param array $varValues
     * @param string $element
     * @param array $options
     *
     */
    private function _replaceBlock($varName, $varValues, $element, $options)
    {
        $varsObject = new \stdClass();
        $varsObject->vars = array();
        $varsObject->options = new \stdClass();

        $varObject = new \stdClass();

        $varObject->var = $varName;
        $varObject->value = $varValues;

        $varsObject->vars[] = $varObject;

        foreach ($options as $key => $value) {
            $varsObject->options->{$key} = $value;
        }

        $varsObject->options->element = $element;

        $this->data->replace[] = $varsObject;
    }

    /**
     * Clone a generic block content
     *
     * @param string $name
     * @param string $element
     * @param integer $repeat
     * @param array $options
     *
     * @return bool
     */
    private function _cloneContent($name, $element, $repeat, $options)
    {
        $cloneObject = new \stdClass();
        $optionsObject = new \stdClass();

        $optionsObject->needle = $name;
        $optionsObject->element = $element;
        $optionsObject->repeat = $repeat;

        foreach ($options as $key => $value) {
            $optionsObject->{$key} = $value;
        }

        $cloneObject->options = $optionsObject;

        $this->data->clone[] = $cloneObject;
        return true;
    }

    /**
     * Removes generic content
     *
     * @param string $name
     * @param string $element
     * @param array $options
     * @return bool
     */
    private function _removeContent($name, $element, $options)
    {
        $removeObject = new \stdClass();
        $optionsObject = new \stdClass();

        $optionsObject->needle = $name;
        $optionsObject->element = $element;

        foreach ($options as $key => $value) {
            $optionsObject->{$key} = $value;
        }

        $removeObject->options = $optionsObject;

        $this->data->remove[] = $removeObject;

        return true;
    }

    /**
     * Custom operations from error code
     *
     * case 1 Authorization incorrect. GenerateAccessToken again.
     * case 10 Temporarily out of service.Sleep and render method try to send the document again
     * default: Multiple errors
     *
     * @param $bodyJson
     */
    private function _processErrorCode($bodyJson)
    {
        $errorCode = $bodyJson->code;
        $errorMessage = $bodyJson->error;

        switch ($errorCode) {
            case 1:
                /*
                 * Authorization incorrect.
                 * generateAccessToken again
                 */
                if ($this->logLevel == 'info') {
                    Log::logger($errorMessage, "warning");
                }
                usleep(1500000);
                $this->generateAccessToken();
                $this->errorFlag = false;
                break;
            case 10:
                /*
                 * Temporarily out of service.
                 * Sleep and render method try to send the document again
                 */
                if ($this->logLevel == 'info') {
                    Log::logger($errorMessage, "warning");
                    Log::logger("Auto try send", "warning");
                }
                sleep(5);
                $this->errorFlag = false;
                break;
            default:
                /*
                 * Multiple errors
                 */
                $this->errorFlag = true;
                if ($this->logLevel != 'none') {
                    Log::logger($errorMessage, "error");
                }

                break;
        }
    }

    /**
     * Parse string to determine if it is a path or base64encoded file
     *
     * @param string $string
     * @return bool|string
     */
    private function _checkTemplate($string)
    {
        if (strpos($string, ".") !== false) {
            // The dot is not a valid base64 cahr so it must be a path
            if (file_exists($string)) {
                return $this->getBase64File($string);
            } else {
                if ($this->logLevel != 'none') {
                    Log::logger("File not found: " . $string, "error");
                }
                return false;
            }

        } else {
            // not a path
            if (base64_encode(base64_decode($string, true)) === $string) {
                return $string;
            } else {
                if ($this->logLevel != 'none') {
                    Log::logger("The provided string is not base64 encoded", "error");
                }
                return false;
            }
        }
    }

    /**
     * Parse string to determinate if it is a path or base64encoded file
     *
     * @param string $outputName
     * @return bool|string
     */
    private function _correctFormat($outputName)
    {
        $file_parts = pathinfo($outputName);
        $extensionOutput = $file_parts['extension'];

        $extensionFile = $this->data->output;

        if ($extensionOutput == $extensionFile) {
            return $outputName;
        } else {
            $outputName = $outputName . "." . $extensionFile;
            return $outputName;
        }
    }
}